$(function() {
  $("#mi-formulario").validate({
    rules: {
      email: {
        required: true,
        email: true
      },
      password: "required",
      password2: {
        required: true,
        equalTo: "#password"
      }
    },
    messages: {
      email: {
        required: 'Ingrese su correo electronico',
        email: 'Formato de correo incorrecto'
      },
      password: {
        required: 'Ingrese su contraseña',
        minlength: 'Minimo no permitido'
      },
      password2: {
        required: 'Reingrese su contraseña',
        equalTo: 'Reingrese su contraseña',
        minlength: 'Minimo no permitido'
      }
    }
  });
});

